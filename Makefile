CC=gcc
CFLAGS=-lm -Wall

gtf: gtf.c
	$(CC) $(CFLAGS) -o gtf gtf.c

clean:
	rm -rf gtf

cleanwin:
	del /f gtf.exe